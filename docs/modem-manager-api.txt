Message hierarchy
=================

Service		org.kop316.vvm
Interface	org.kop316.vvm.ModemManager
Object path	/org/kop316/vvm


Methods

	ChangeSettings
	    Change a setting in the Modem Manager Plugin
	    Gvariant Input Format String    ((sv))

	ViewSettings
	    Change all Modem Manager related settings
	    Gvariant Output Format String    (a{sv})

	SyncVVM
            This forces a sync of all of the emails from the IMAP server.

Signals
	ProvisionStatusChanged
	    The provisioning status from the carrier has changed.
	    Gvariant Input Format String    ((sv))

    SettingsChanged()
        mmsd-tng changed the MMS settings based on an auto configureation

        Gvariant Output Format String    (sss)
        	standard: The VVM standard
        	destination_number: The destination number
        	carrier_prefix: The carrier prefix


Properties	None
